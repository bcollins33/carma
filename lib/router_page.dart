import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_stuff/login_page.dart';
import 'package:flutter_stuff/test_page.dart';
import 'package:provider/provider.dart';

import 'firebase.dart';

class RouterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<FirebaseState>(builder: (context, fbState, child) {
      if (fbState.isSignedIn()) {
        print('is signed in');
        return TestPage();
      } else {
        print('is signed out');
        return LoginPage();
      }
    });
  }
}
