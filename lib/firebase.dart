import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';

class FirebaseState extends ChangeNotifier {

  Future<void> signIn(String email, String password) async {
    try {
      print("Trying to sign in email:$email");
      await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);
      notifyListeners();
    } on Exception catch (e) {
      print(e);
    }
  }

  Future<void> signInAnonymously() async {
    try {
      await FirebaseAuth.instance.signInAnonymously();
      notifyListeners();
    } catch (e) {
      print(e); // TODO: show dialog with error
    }
  }

  Future<void> signOut() async {
    try {
      await FirebaseAuth.instance.signOut();
      notifyListeners();
    } catch (e) {
      print(e); // TODO: show dialog with error
    }
  }


  bool isSignedIn(){
    return getUser() != null;
  }

  User getUser(){
    return FirebaseAuth.instance.currentUser;
  }

}
