import 'package:flutter/material.dart';
import 'package:flutter_stuff/firebase.dart';
import 'package:flutter_stuff/login_page.dart';
import 'package:flutter_stuff/router_page.dart';
import 'package:provider/provider.dart';
import 'package:firebase_core/firebase_core.dart';

void main() {
  runApp(ChangeNotifierProvider(
    create: (context) => FirebaseState(),
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Carma',
      theme: ThemeData(
        primaryColor: Colors.indigo,
        accentColor: Colors.tealAccent,
        accentTextTheme: TextTheme(button: TextStyle(color: Colors.white)),
        brightness: Brightness.dark,
        textTheme: TextTheme(
            headline1: TextStyle(fontSize: 44.0, fontWeight: FontWeight.bold)),
        buttonTheme: ButtonThemeData(
            buttonColor: Colors.tealAccent,
            textTheme:
                ButtonTextTheme.primary //this auto selects the right color
            ),
      ),
      home: FirebaseStarter(),
    );
  }
}

class FirebaseStarter extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      // Initialize FlutterFire
      future: Firebase.initializeApp(),
      builder: (context, snapshot) {
        // Check for errors
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }

        // Once complete, show your application
        if (snapshot.connectionState == ConnectionState.done) {
          return RouterPage();
        }

        // Otherwise, show something whilst waiting for initialization to complete
        return  Scaffold(
          body: Center(
            child: CircularProgressIndicator(),
          ),
        );
      },
    );
  }
}
