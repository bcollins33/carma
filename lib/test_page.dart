import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'firebase.dart';

class TestPage extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: RaisedButton(
          child: Text("Logout"),
          onPressed: () async {
            await Provider.of<FirebaseState>(context, listen: false)
                .signOut();
          },
        ),
      ),
    );
  }
}