import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyCustomForm extends StatefulWidget{

  MyCustomFormState formState;

  @override
  State createState() {
    formState = MyCustomFormState();
    return formState;
  }

  void validate(){
    formState?.validate();
  }
}

class MyCustomFormState extends State<MyCustomForm>{
  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: Column(
        children: [
          TextFormField(
            validator: (value){
              if(value.isEmpty){
                return 'Please enter some text';
              }
              return null;
            },
          )
        ],
      ),
    );
  }

  void validate(){
    if(formKey.currentState.validate()){
      Scaffold.of(context).showSnackBar(
          SnackBar(content: Text('Processing Data')));
    }
  }
}